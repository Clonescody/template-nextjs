import React from 'react';
import PropTypes from 'prop-types';

function Book(props) {
  const { book } = props;
  const { id, title, description, author } = book;
  return (
    <div height={100} width={200} key={id}>
      <h3>{title}</h3>
      <h4>by {author}</h4>
      <p>{description}</p>
      {/* <p>{publicationDate}</p> */}
    </div>
  );
}

Book.defaultProps = {
  book: {
    id: null,
    title: null,
    description: null,
    author: null,
  },
};

Book.propTypes = {
  book: {
    id: PropTypes.number,
    title: PropTypes.string,
    description: PropTypes.string,
    author: PropTypes.string,
  },
};

export default Book;
