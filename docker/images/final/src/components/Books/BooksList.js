import React from 'react';
import PropTypes from 'prop-types';
import Book from './Book';

function BooksList(props) {
  const { books } = props;
  return (
    <div>
      <h2>Books list</h2>
      <ul>
        {books.map(book => (
          <li key={book.id}>
            <Book book={book} />
          </li>
        ))}
      </ul>
    </div>
  );
}

BooksList.defaultProps = {
  books: null,
};

BooksList.propTypes = {
  books: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      description: PropTypes.string,
      author: PropTypes.string,
    })
  ),
};

export default BooksList;
