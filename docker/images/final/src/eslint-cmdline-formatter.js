// eslint-disable-next-line import/no-extraneous-dependencies
const chalk = require('chalk');
// eslint-disable-next-line import/no-extraneous-dependencies
const logSymbols = require('log-symbols');

let globalErrors = 0;
let globalWarnings = 0;

function minimalFormatter(result) {
  if (!result.messages || !result.messages.length) {
    return '';
  }

  globalErrors += result.errorsCount;
  globalWarnings += result.warningsCount;

  let errorsOutput = '';
  let localErrors = 0;

  result.messages.forEach(issue => {
    if (issue.severity === 2) {
      localErrors += 1;
      const symbol = chalk.red(logSymbols.error);
      const line = chalk.yellow.bold.underline(`Line ${issue.line}:${issue.column}`);
      const message = chalk.white(issue.message);
      const rule = chalk.grey.bold(issue.ruleId);
      errorsOutput += `${symbol}    ${line}    ${message}    ${rule}\n`;
    }
  });

  let output = '';
  if (result.errorsCount > 0) {
    output += `${chalk.green.underline(result.source)}\n`;
    if (result.errorsCount !== localErrors) {
      output += chalk.red.bold.underline(
        'Formatter error : file error count is different hant the one in json report!'
      );
    }
    output += errorsOutput;
  }

  return output === '' ? '' : `\n${output}`;
}

module.exports = results => {
  const report = results.reduce((output, result) => {
    // eslint-disable-next-line no-param-reassign
    output += minimalFormatter({
      messages: result.messages,
      source: result.filePath,
      errorsCount: result.errorCount,
      warningsCount: result.warningCount,
    });
    return output;
  }, '');

  if (report === '') {
    let success = `\n${chalk.white.underline('EsLint Report:')}\n${chalk.green(
      logSymbols.success
    )}  ${chalk.white(`No errors on ${results.length} files checked`)}\n`;
    if (globalWarnings > 0) {
      success += `${chalk.yellow(logSymbols.warning)}  But there are ${chalk.yellow.bold.underline(
        `${globalWarnings} warnings`
      )}.\n\n`;
    }
    return success;
  }

  let counts = `${chalk.white.underline('EsLint Report:')}\n`;
  counts += `${chalk.green(logSymbols.success)}  ${chalk.bold(results.length)} files checked\n`;
  counts += `${chalk.red(logSymbols.error)}  ${chalk.red.bold(globalErrors)} ${chalk.red(
    'errors'
  )}\n`;
  counts += `${chalk.yellow(logSymbols.warning)}  ${chalk.yellow.bold(
    globalWarnings
  )} ${chalk.yellow('warnings')}\n`;
  return `${report}\n${counts}\n`;
};
