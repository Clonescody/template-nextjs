// eslint-disable-next-line import/no-extraneous-dependencies
const chalk = require('chalk');
// eslint-disable-next-line import/no-extraneous-dependencies
const logSymbols = require('log-symbols');

function minimalFormatter(result) {
  if (!result.messages || !result.messages.length) {
    return '';
  }

  let errorsOutput = '';
  let localErrors = 0;

  result.messages.forEach(issue => {
    if (issue.severity === 2) {
      localErrors += 1;
      const symbol = chalk.red(logSymbols.error);
      const line = chalk.yellow.bold.underline(`Line ${issue.line}:${issue.column}`);
      const message = chalk.white(issue.message);
      const rule = chalk.grey.bold(issue.ruleId);
      errorsOutput += `${symbol}    ${line}    ${message}    ${rule}\n`;
    }
  });

  let output = '';
  if (result.errorsCount > 0) {
    output += `${chalk.green.underline(result.source)}\n`;
    if (result.errorsCount !== localErrors) {
      output += chalk.red.bold.underline(
        'Formatter error : file error count is different than the one in json report!'
      );
    }
    output += errorsOutput;
  }

  return output === '' ? '' : `\n${output}`;
}

module.exports = results => {
  const report = results.reduce((output, result) => {
    // eslint-disable-next-line no-param-reassign
    output += minimalFormatter({
      messages: result.messages,
      source: result.filePath,
      errorsCount: result.errorCount,
      warningsCount: result.warningCount,
    });
    return output;
  }, '');

  return report;
};
