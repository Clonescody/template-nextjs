module.exports = plop => {
  plop.setHelper('ucFirst', txt => `${txt.slice(0, 1).toUpperCase()}${txt.slice(1)}`);
  plop.setHelper('lcFirst', txt => `${txt.slice(0, 1).toLowerCase()}${txt.slice(1)}`);
  // create your generators here
  plop.setGenerator('classComponent', {
    description:
      'Will create a react class component js file, the styled css file and unit test file',
    prompts: [
      {
        type: 'input',
        name: 'fullPath',
        message: 'type full path from root to component file (from root; ex: components/myFolder/)',
      },
      {
        type: 'input',
        name: 'name',
        message: 'type component name (ex: MyComponent)',
      },
    ],
    actions: [
      {
        type: 'add',
        path: './{{fullPath}}/{{ucFirst name}}.js',
        templateFile: '_tools/PlopTemplates/classComponent.hbs',
      },
      {
        type: 'add',
        path: './{{fullPath}}/{{ucFirst name}}.spec.js',
        templateFile: '_tools/PlopTemplates/unitTest.hbs',
      },
      {
        type: 'add',
        path: './{{fullPath}}/Styled{{ucFirst name}}.js',
        templateFile: '_tools/PlopTemplates/styledComponent.hbs',
      },
    ],
  });
  plop.setGenerator('pureComponent', {
    description:
      'Will create a react arrow function component js file, the css file and unit test file',
    prompts: [
      {
        type: 'input',
        name: 'fullPath',
        message: 'type full path from root to component file (from root; ex: components/myFolder/)',
      },
      {
        type: 'input',
        name: 'name',
        message: 'type component name (ex: MyComponent)',
      },
    ],
    actions: [
      {
        type: 'add',
        path: './{{fullPath}}/{{ucFirst name}}.js',
        templateFile: '_tools/PlopTemplates/pureComponent.hbs',
      },
      {
        type: 'add',
        path: './{{fullPath}}/{{ucFirst name}}.spec.js',
        templateFile: '_tools/PlopTemplates/unitTest.hbs',
      },
      {
        type: 'add',
        path: './{{fullPath}}/Styled{{ucFirst name}}.js',
        templateFile: '_tools/PlopTemplates/styledComponent.hbs',
      },
    ],
  });
};
