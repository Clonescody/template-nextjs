module.exports = {
  appName: 'template-nextjs',
  apiBaseUrl: process.env.API_URL,
  apiSecretKey: process.env.SECRET_API_KEY,
};
