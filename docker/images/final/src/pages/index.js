import React from 'react';
import PropTypes from 'prop-types';
import BooksList from '../components/Books/BooksList';
import getBooks from '../api/books';

function Home(props) {
  const { books } = props;
  return (
    <div>
      <p>Book Library template using the API Platform template</p>
      <p>Made with Docker and BitBucket | GitLab CI</p>
      {books.length > 0 ? (
        <BooksList books={books} />
      ) : (
        <img
          alt="loading"
          src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"
        />
      )}
    </div>
  );
}

Home.defaultProps = {
  books: null,
};

Home.propTypes = {
  books: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      description: PropTypes.string,
      author: PropTypes.string,
    })
  ),
};

Home.getInitialProps = async () => {
  const result = await getBooks();
  const datas = await result.json();
  return { books: datas['hydra:member'].map(entity => entity) };
};

export default Home;
