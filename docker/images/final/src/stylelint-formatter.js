// eslint-disable-next-line import/no-extraneous-dependencies
const chalk = require('chalk');
// eslint-disable-next-line import/no-extraneous-dependencies
const logSymbols = require('log-symbols');

let errorsCount = 0;
let warningsCount = 0;
let deprecationsCount = 0;

function deprecationsFormatter(deprecations) {
  if (!deprecations || !deprecations.length) {
    return '';
  }

  return deprecations.reduce((output, warning) => {
    deprecationsCount += 1;
    /* eslint-disable no-param-reassign */
    output += chalk.yellow.bold('>> Deprecation Warning: ');
    output += chalk.yellow(warning.text);
    if (warning.reference) {
      output += chalk.yellow(' See: ');
      output += chalk.green.underline(warning.reference);
    }
    /* eslint-enable no-param-reassign */
    return `${output}\n`;
  }, '\n');
}

function minimalFormatter(result) {
  if (!result.messages || !result.messages.length) {
    return '';
  }

  let errorsOutput = '';
  let localErrors = 0;
  // let localWarnings = 0;
  result.messages.forEach(issue => {
    if (issue.severity === 'warning') {
      // localWarnings += 1;
      warningsCount += 1;
    } else if (issue.severity === 'error') {
      localErrors += 1;
      errorsCount += 1;

      const symbol = chalk.red(logSymbols.error);
      const line = chalk.yellow.bold.underline(`Line ${issue.line}:${issue.column}`);
      const message = chalk.white(issue.text);
      const rule = chalk.grey.bold(issue.rule);
      errorsOutput += `${symbol}    ${line}    ${message}    ${rule}\n`;
    }
  });

  let output = '';
  if (localErrors > 0) {
    output += `${chalk.green.underline(result.source)}\n`;
    output += errorsOutput;
  }

  return output === '' ? '' : `\n${output}`;
}

module.exports = results => {
  const report = results.reduce((output, result) => {
    /* eslint-disable no-param-reassign */
    output += deprecationsFormatter(result.deprecations);
    output += minimalFormatter({
      messages: result.warnings,
      source: result.source,
    });
    /* eslint-enable no-param-reassign */
    return output;
  }, '');

  if (report === '') {
    let success = `\n${chalk.white.underline('StyleLint Report:')}\n${chalk.green(
      logSymbols.success
    )}  ${chalk.white(`No errors on ${results.length} files checked`)}\n`;
    if (warningsCount > 0 || deprecationsCount > 0) {
      success += `${chalk.yellow(logSymbols.warning)}  But there are ${chalk.yellow.bold.underline(
        `${warningsCount} warnings`
      )} and ${chalk.magenta.bold.underline(`${deprecationsCount} deprecates`)}.\n\n`;
    }
    return success;
  }

  let counts = `${chalk.white.underline('StyleLint Report:')}\n`;
  counts += `${chalk.green(logSymbols.success)}  ${chalk.bold(results.length)} files checked\n`;
  counts += `${chalk.red(logSymbols.error)}  ${chalk.red.bold(errorsCount)} ${chalk.red(
    'errors'
  )}\n`;
  counts += `${chalk.yellow(logSymbols.warning)}  ${chalk.yellow.bold(
    warningsCount
  )} ${chalk.yellow('warnings')}\n`;
  counts += `${chalk.magenta(logSymbols.info)}  ${chalk.magenta.bold(
    deprecationsCount
  )} ${chalk.magenta('deprecations')}\n`;
  return `${report}\n${counts}\n`;
};
