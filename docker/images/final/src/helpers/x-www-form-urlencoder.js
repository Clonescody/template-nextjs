export default function xWwwFormUrlEncode(obj) {
  return Object.keys(obj)
    .reduce((params, key) => {
      const values = !Array.isArray(obj[key]) ? [obj[key]] : obj[key];

      // We check if the array has more than one item to append [] to the key
      // in the url encoded form like this : key[]=value
      const hasMoreThanOneItem = values.length > 1;

      return params.concat(
        values.map(
          value =>
            `${encodeURIComponent(key)}${hasMoreThanOneItem ? '[]' : ''}=${encodeURIComponent(
              value
            )}`
        )
      );
    }, [])
    .join('&');
}
