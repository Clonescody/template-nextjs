module.exports = {
    env: {
        browser: true,
        node: true,
        es6: true,
        jquery: true,
        'jest/globals': true,
    },
    globals: {
        _: false,
        Backbone: false,
    },
    extends: ['airbnb', 'prettier', 'prettier/react'],
    parser: 'babel-eslint',
    parserOptions: {
        ecmaFeatures: {
            ecmaVersion: 6,
            jsx: true,
        },
        sourceType: 'module',
    },
    plugins: ['jest', 'react', 'jsx-a11y', 'prettier'],
    rules: {
        'prettier/prettier': 'error',
        'linebreak-style': ['error', 'unix'],
        quotes: ['warn', 'single'],
        semi: ['off', 'never'],
        'eol-last': ['error', 'always'],
        'no-underscore-dangle': 0,
        'max-len': ['error', { code: 255 }],
        // Disable for backbone
        'prefer-rest-params': ['off'],
        'prefer-arrow-callback': ['warn'],
        'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
        'react/prefer-stateless-function': [1, { ignorePureComponents: true }],
        'jsx-a11y/label-has-for': [
            2,
            {
                component: ['Label'],
                required: { some: ['id', 'nesting'] },
            },
        ],
    },
};
