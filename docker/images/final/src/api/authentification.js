import fetch from 'isomorphic-unfetch';
import config from '../config';
import xWwwFormUrlEncode from '../helpers/x-www-form-urlencoder';

const { apiBaseUrl, apiSecretKey } = config;

export async function fetchData(method = 'GET', uri, parameters, headers = {}) {
  let endpointUrl = uri;
  const finalHeaders = {
    ...headers,
    'SECRET-API-KEY': apiSecretKey,
    'Content-Type': 'application/json',
  };

  if (method === 'GET') {
    if (parameters && Object.keys(parameters).length) {
      endpointUrl += `?${xWwwFormUrlEncode(parameters)}`;
    }
  }
  return fetch(endpointUrl, {
    method,
    headers: finalHeaders,
    body: method === 'GET' ? null : JSON.stringify(parameters),
  });
}

export async function get(endpoint, parameters = {}, headers = {}) {
  return fetchData('GET', `${apiBaseUrl}/${endpoint}`, parameters, headers);
}

export async function post(endpoint, parameters, headers = {}) {
  return fetchData('POST', `${apiBaseUrl}/${endpoint}`, parameters, headers);
}

export async function put(endpoint, parameters, headers = {}) {
  return fetchData('PUT', `${apiBaseUrl}/${endpoint}`, parameters, headers);
}

export async function del(endpoint, parameters, headers = {}) {
  return fetchData('DELETE', `${apiBaseUrl}/${endpoint}`, parameters, headers);
}

export async function getEntityWithURI(entityURI) {
  return fetchData('GET', `${apiBaseUrl}${entityURI}`);
}
