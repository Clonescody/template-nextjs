import { get } from './authentification';

export default function getBooks() {
  return get('books');
}
