COM_COLOR   = \033[0;34m
OBJ_COLOR   = \033[0;36m
OK_COLOR    = \033[0;32m
ERROR_COLOR = \033[0;31m
WARN_COLOR  = \033[0;33m
NO_COLOR    = \033[m

export PROJECT=template-nextjs
export IMAGE=node-service

LOCAL_USER_ID=1010
LOCAL_GROUP_ID=1010

DOCKER_RUN_DEV=docker run -ti --rm -p 3000:3000 --env-file=dev.env --name=${PROJECT}-${IMAGE} -v `pwd`/src:/usr/src/app ${PROJECT}/${IMAGE}-dev

# Docker

.PHONY: build-final
build-final:
	docker build -t ${PROJECT}/${IMAGE} docker/images/final

.PHONY: build
build:
	docker build --build-arg LOCAL_USER_ID=${LOCAL_USER_ID} --build-arg LOCAL_GROUP_ID=${LOCAL_GROUP_ID} -t ${PROJECT}/${IMAGE}-dev docker/images/dev

.PHONY: start
start: yarn-start

.PHONY: start-final
start-final: build-final
	docker run -d --rm -p 3030:3030 --env-file=dev.env --name=${PROJECT}-${IMAGE}-final ${PROJECT}/${IMAGE}

.PHONY: run-sandbox
run-sandbox: yarn-sandbox

# Yarn

.PHONY: yarn-start
yarn-start: build yarn-install yarn-build
	${DOCKER_RUN_DEV} yarn start-dev

.PHONY: yarn-sandbox
yarn-sandbox: build yarn-install yarn-build
	${DOCKER_RUN_DEV} yarn sandbox

.PHONY: yarn-install
yarn-install:
	${DOCKER_RUN_DEV} yarn install

.PHONY: yarn-build
yarn-build:
	${DOCKER_RUN_DEV} yarn build

.PHONY: yarn-export
yarn-export: yarn-build
	${DOCKER_RUN_DEV} yarn export

.PHONY: yarn-eslint
yarn-eslint:
	${DOCKER_RUN_DEV} yarn eslint

.PHONY: yarn-stylelint
yarn-stylelint:
	${DOCKER_RUN_DEV} yarn stylelint

# Docker

.PHONY: exec
exec:
	docker exec -ti ${PROJECT}-${IMAGE} /bin/bash

.PHONY: stop
stop:
	docker stop ${PROJECT}-${IMAGE}

.PHONY: logs
logs:
	docker logs -f ${PROJECT}-${IMAGE}

.PHONY: push-test
push-test:
ifndef REGISTRY
	@echo "$(ERROR_COLOR)Need registry$(NO_COLOR)"
	@echo "$(ERROR_COLOR)Try: \"REGISTRY=my.registry make push\"$(NO_COLOR)"
	@exit
endif
ifdef REGISTRY
	echo "$(OK_COLOR)Registry: ${REGISTRY}$(NO_COLOR)"
	docker tag ${PROJECT}/${IMAGE} ${REGISTRY}/${PROJECT}/${IMAGE}:test
	docker push ${REGISTRY}/${PROJECT}/${IMAGE}:test
endif

.PHONY: push
push:
	echo ${DOCKER_PASSWORD} | docker login --username ${DOCKER_USERNAME} --password-stdin ${REGISTRY}

	#version
	docker tag ${PROJECT}/${IMAGE} ${REGISTRY}/${PROJECT}/${IMAGE}:${VERSION}
	docker push ${REGISTRY}/${PROJECT}/${IMAGE}:${VERSION}

	#short
	docker tag ${PROJECT}/${IMAGE} ${REGISTRY}/${PROJECT}/${IMAGE}:${VERSION_SHORT}
	docker push ${REGISTRY}/${PROJECT}/${IMAGE}:${VERSION_SHORT}

	#latest
	docker tag ${PROJECT}/${IMAGE} ${REGISTRY}/${PROJECT}/${IMAGE}:latest
	docker push ${REGISTRY}/${PROJECT}/${IMAGE}:latest
